var User = require('../models/user');

exports.ensureUser = function(req, res, next) {

	var userId = req.body.userId == null ? userId = req.params.userId : req.body.userId;


    User.findOne({_id: userId }, function(error, user){

        if (error) {
            return next(error);
        }

        if (!user) {
            return res.send('404 - User Not Found', 404);
        }

        req.user = user;

        next();
    });

}