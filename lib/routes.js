var controllers = require('../controllers'),
	singalingsHelper = require('./singalingsHelper');

var User = require('../models/user');
var Auth = require('./authentication');

module.exports = function(app, passport){
	app.get('/', controllers.home.index);

    app.get("/login", function(req, res){ 
		res.render("login");
	});

	app.post("/login" 
		,passport.authenticate('local',{
			successRedirect : "/profile",
			failureRedirect : "/login",
		})
	);


	app.get('/register', controllers.home.register);

	app.get('/address', controllers.home.address);   

	app.get('/halloffame', controllers.home.hallOfFame);  

	app.get('/music', controllers.home.music);  

	app.post('/register/confirmation',
		controllers.home.registerSave);

	app.get('/profile', Auth.isAuthenticated , function(req, res){ 
		res.render("profile", { user : req.user});
	});
}
