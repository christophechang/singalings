
/**
 * Module dependencies.
 */

var express = require('express'),
	http = require('http'),
	path = require('path'),
	hbs = require('hbs'),
    passport = require("passport"),
	mongoose = require('mongoose'),
    flash = require("connect-flash");

require('./lib/passport')(passport);

var	app = express();

// all environments
app.set('port',  3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'html');
app.engine('html', require('hbs').__express);
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(app.router);
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());
app.use(express.static(path.join(__dirname, 'assets')));

hbs.registerPartials(__dirname + '/views/partials');

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

require('./lib/routes.js')(app, passport);
require('./lib/db.js')();

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});