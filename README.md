# Singalings

Version v0.1

## Background

Singalings are a group of characters dreamed up by my son Claude.

He wanted to build his first web site so I put together this small nodejs project for him to try out his new web skills.

## How To Run Singalings

* You'll need [Node.js](nodejs.org) and [MongoDB](http://www.mongodb.org/) installed on your machine
* In the project directory run `npm install` this will install any packages needed by the project
* Make sure you start your Mongo process before trying to run the server
* `node app.js` will start the project (or use [nodemon](https://github.com/remy/nodemon) to avoid manual server restarts)
* I have also implemented a task runner called Grunt. If you have grunt-cli installed, you can just run by typing 'grunt'.
* This will start the database, add a sass watch and then run nodemon.


## Contact

If you have any questions, suggestions or feedback at all then get in touch with singalings@changsta.com
