var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var hash = require('../util/hash'); 

var userSchema = new Schema({
	firstName : String,
	lastName : String,
	email: String,
    salt: String,
    hash: String,
	active: Boolean
});


userSchema.virtual('fullName').get(function () {
	return this.firstName + ' ' + this.lastName;
});

userSchema.statics.isValidUserPassword = function(email, password, done) {
	this.findOne({email : email}, function(err, user){
		// if(err) throw err;
		if(err) return done(err);
		if(!user) return done(null, false, { message : 'Incorrect email.' });
		hash(password, user.salt, function(err, hash){
			if(err) return done(err);
			if(hash == user.hash) return done(null, user);
			done(null, false, {
				message : 'Incorrect password'
			});
		});
	});
};

module.exports = mongoose.model('user', userSchema);