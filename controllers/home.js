
var User = require('../models/user');
var hash = require('../util/hash'); 
/*
 * GET home page.
 */

// GET: /
exports.index = function(req, res) {
	res.render('index', {home: true});
};

exports.profile = function(req, res) {
	res.render('profile', {profile: true});
};

exports.login = function(req, res) {
	res.render('login', {login: true});
};

exports.address = function(req, res) {
	res.render('address', {address: true});	
}

exports.register = function(req, res) {

	User.find({}, function (err, users) {
		res.render('register', {register: true, users: users});	
	});
}

exports.hallOfFame = function(req, res, next) {
	res.render('hallOfFame', {hallOfFame: true});	
}

exports.music = function(req, res, next) {
	res.render('music', {music: true});	
}

exports.registerSave = function(req, res, next) {


    hash(req["body"]["password"], function(err, salt, hash){
		if(err) throw err;
		// if (err) return done(err);


        var user = new User({
		    firstName : req.body.firstName,
		    lastName : req.body.lastName,
		    email : req.body.email,
            salt : salt,
			hash : hash,
		    active: true
	    }); 


	    user.save(function(error, user){
		    if(error){
			    console.log('error saving', error);
			    return next(error);
		    }

	        User.find({}, function (err, users) {
		        res.render('profile', {user: user});	
	        });
	    });	

	});

}